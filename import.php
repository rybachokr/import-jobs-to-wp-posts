<?php
//WARN: use this inside WP installation
//use wget by url for example "wget http://www.jobscale.com/search/export/region/bayern/query/koch" to receive jobs json

function importJobs($fileJson = 'koch')
{
    $string = file_get_contents($fileJson);

    $string = preg_replace("/[\r\n]+/", " ", $string);
    $json = utf8_encode($string);
    $json = json_decode($json, true);

    foreach($json as $job)
    {
        $my_post = array(
            'post_title' => $job['title'],
            'post_date' => $job['postedDate'],
            'post_content' => $job['description'],
            'post_status' => 'publish',
            'post_type' => 'post',
        );
        $the_post_id = wp_insert_post( $my_post );


        update_post_meta( $the_post_id, 'email', $job['email'] );
        update_post_meta( $the_post_id, 'profession', $job['profession'] );
        update_post_meta( $the_post_id, 'company', $job['company'] );
        update_post_meta( $the_post_id, 'phone', $job['phone'] );
        update_post_meta( $the_post_id, 'address', $job['address'] );
        update_post_meta( $the_post_id, 'city', $job['city'] );
    }
}